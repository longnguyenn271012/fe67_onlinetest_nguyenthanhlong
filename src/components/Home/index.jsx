import React, { Component } from "react";
import Questions from "../Questions";
import { fetchQuestions } from "../../store/actions/questions";
import { connect } from "react-redux";

class Home extends Component {
  render() {
    return (
      <div>
        <h1>Online Test</h1>
        <div
          style={{
            width: "60%",
            margin: "0 auto",
            marginBottom: "20px",
            textAlign: "justify",
          }}
        >
          <Questions />
        </div>
      </div>
    );
  }

  componentDidMount() {
    this.props.dispatch(fetchQuestions);
  }
}

export default connect()(Home);
