import React, { Component } from "react";
import { connect } from "react-redux";
import { createAction } from "../../store/actions";
import { actionType } from "../../store/actions/type";

class SelectedQuestion extends Component {
  constructor(props) {
    super(props);
    this.state = {
      answerValue: {
        questionId: "",
        answer: {
          content: "",
          exact: false,
        },
      },
    };
  }

  handleChange = (event) => {
    //Dựa vào id trong event, Tìm đáp án được lựa, từ đó lấy content, exact
    const foundAnswer = this.props.item.answers.find((answerItem) => {
      return answerItem.id === event.target.id;
    });

    //Do setState là async nên nếu dispatch ngay tại đây sẽ dùng state cũ
    //Sử dụng cấu trúc dưới để sau khi set xong answerValue sẽ chạy hàm dispatch
    this.setState(
      {
        answerValue: {
          ...this.state.answerValue,
          questionId: event.target.name,
          answer: {
            content: foundAnswer.content,
            exact: foundAnswer.exact,
          },
        },
      },
      () => {
        this.props.dispatch(
          createAction(actionType.SET_ANSWER, this.state.answerValue)
        );
      }
    );
  };

  renderSelectedQuestion = () => {
    const questionId = this.props.item.id;
    return this.props.item.answers.map((answerItem) => {
      const { id, content } = answerItem;
      return (
        <div key={id}>
          <input
            type="radio"
            id={id}
            name={questionId}
            onChange={this.handleChange}
          ></input>
          <label htmlFor={id}>{content}</label>
        </div>
      );
    });
  };

  render() {
    const { id, content } = this.props.item;
    return (
      <form
        key={id}
        style={{ width: "80%", margin: "0 auto", fontSize: "22px" }}
      >
        <h2>
          Câu {id}: {content}
        </h2>

        {this.renderSelectedQuestion()}
      </form>
    );
  }
}

export default connect()(SelectedQuestion);
