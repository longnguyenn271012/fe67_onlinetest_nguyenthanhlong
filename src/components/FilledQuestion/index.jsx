import React, { Component } from "react";
import { createAction } from "../../store/actions";
import { actionType } from "../../store/actions/type";
import { connect } from "react-redux";

class FilledQuestion extends Component {
  constructor(props) {
    super(props);
    this.state = {
      answerValue: {
        questionId: "",
        answer: {
          content: "",
          exact: false,
        },
      },
    };
  }

  handleChange = (event) => {
    //Dựa vào id trong event, Tìm đáp án được lựa, từ đó lấy content, exact
    const foundAnswer = this.props.item.answers.find((answerItem) => {
      return answerItem.id === event.target.id;
    });

    this.setState(
      {
        answerValue: {
          ...this.state.answerValue,
          questionId: event.target.id,
          answer: {
            content: event.target.value,
            exact:
              foundAnswer.content.toLowerCase() ===
              event.target.value.toLowerCase(),
          },
        },
      },
      () => {
        this.props.dispatch(
          createAction(actionType.SET_ANSWER, this.state.answerValue)
        );
      }
    );
  };

  renderFilledQuestion = () => {
    const { id } = this.props.item;
    return (
      <div key={id}>
        <input
          type="text"
          id={id}
          onChange={this.handleChange}
          style={{ width: "100%", fontSize: "20px" }}
        ></input>
      </div>
    );
  };
  render() {
    const { id, content } = this.props.item;
    return (
      <form
        key={id}
        onSubmit={this.handleSubmit}
        style={{ width: "80%", margin: "0 auto", fontSize: "22px" }}
      >
        <h2>
          Câu {id}: {content}
        </h2>

        {this.renderFilledQuestion()}
      </form>
    );
  }
}

export default connect()(FilledQuestion);
