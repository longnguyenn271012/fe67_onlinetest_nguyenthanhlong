import React, { Component } from "react";
import { connect } from "react-redux";
import SelectedQuestion from "../SelectedQuestion";
import FilledQuestion from "../FilledQuestion";

class Questions extends Component {
  handleSubmit = () => {
    let result = 0;
    for (let answerItem of this.props.answers) {
      console.log(answerItem);
      if (answerItem.answer.exact) {
        result++;
      }
    }

    alert("Số điểm của bạn là: " + result + "/8 !!!");
  };

  render() {
    return (
      <div>
        {this.props.questions.map((item) => {
          if (item.questionType === 1) {
            return (
              <div key={item.id}>
                <SelectedQuestion item={item} />
              </div>
            );
          } else {
            return (
              <div key={item.id}>
                <FilledQuestion item={item} />
              </div>
            );
          }
        })}

        <button
          onClick={this.handleSubmit}
          style={{ textAlign: "center", width: "20%", marginLeft: "40%" }}
        >
          SUBMIT
        </button>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  questions: state.questions.questionsList,
  answers: state.questions.answersList,
});

export default connect(mapStateToProps)(Questions);
