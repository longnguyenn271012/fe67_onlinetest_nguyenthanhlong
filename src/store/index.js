import { combineReducers, createStore, applyMiddleware, compose } from "redux"
import questions from "./reducers/questions";
import thunk from "redux-thunk";

const reducer = combineReducers({
    questions,
});

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export default createStore(reducer, composeEnhancers(applyMiddleware(thunk)));