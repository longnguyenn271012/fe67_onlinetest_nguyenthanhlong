import { actionType } from "../actions/type";

const initialState = {
  questionsList: [],
  answersList: [],
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case (action.type = actionType.SET_QUESTIONS): {
      state.questionsList = action.payload;
      return { ...state };
    }

    case (action.type = actionType.SET_ANSWER): {
      const cloneAnswersList = [...state.answersList];
      const foundIndex = cloneAnswersList.findIndex((item) => {
        return item.questionId === action.payload.questionId;
      });
      if (foundIndex === -1) {
        const answerItem = action.payload;
        cloneAnswersList.push(answerItem);
      } else {
        cloneAnswersList[foundIndex] = action.payload;
      }
      state.answersList = cloneAnswersList;
      return { ...state };
    }
    
    default:
      return state;
  }
};

export default reducer;
